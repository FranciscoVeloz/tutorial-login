import cors from "cors";
import morgan from "morgan";
import express, { urlencoded } from "express";

const app = express();

app.use(cors());
app.use(morgan("dev"));
app.use(express.json());
app.use(urlencoded({ extended: false }));

app.get("/ping", (_req, res) => res.json("pong"));

app.post("/login", (req, res) => {
  const { email, password } = req.body;

  if (email !== "francisco@gmail.com")
    return res.status(401).json("Email no encontrado");

  if (password !== "12345")
    return res.status(401).json("Contraseña incorrecta");

  res.status(200).json({
    token: "123467353453453453453451234",
    user: {
      email: "francisco@gmail.com",
      username: "Francisco",
      role: "admin",
    },
  });
});

app.listen(5000, () => console.log("Server on port http://localhost:5000"));
