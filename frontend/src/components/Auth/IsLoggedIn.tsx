import React, { useContext } from "react";
import AuthContext from "../../context/AuthContext";
import { Navigate } from "react-router-dom";

interface props {
  view: React.ElementType;
}

const IsLoggedIn = ({ view: View }: props) => {
  const { user } = useContext(AuthContext);

  //Validamos si el token ya expiro o no
  //   const validateSession = async () => {
  //     if (user !== null) {
  //       const data = await get(user!.email!);
  //       if (!data.status) {
  //         helpers.swalError(data.message!);
  //         logout(); //obtener del auth context
  //       }
  //     }
  //   };

  //Use effect
  //   useEffect(() => {
  //     validateSession();
  //   });

  if (user === null) return <Navigate to={"/login"} replace={true} />;
  return <View />;
};

export default IsLoggedIn;
