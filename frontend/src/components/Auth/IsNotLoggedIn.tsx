import React, { useContext } from "react";
import AuthContext from "../../context/AuthContext";
import { Navigate } from "react-router-dom";

interface props {
  view: React.ElementType;
}

const IsNotLoggedIn = ({ view: View }: props) => {
  const { user } = useContext(AuthContext);

  if (user === null) return <View />;
  return <Navigate to="/" replace={true} />;
};

export default IsNotLoggedIn;
