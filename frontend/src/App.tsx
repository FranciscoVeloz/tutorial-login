import Layout from "./components/Layout";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Protected from "./pages/Protected";
import { BrowserRouter, Routes, Route } from "react-router-dom";

import useAuth from "./hooks/useAuth";
import AuthContext from "./context/AuthContext";
import IsNotLoggedIn from "./components/Auth/IsNotLoggedIn";
import IsLoggedIn from "./components/Auth/IsLoggedIn";

const App = () => {
  const auth = useAuth();

  return (
    <BrowserRouter>
      <AuthContext.Provider value={auth}>
        <Layout>
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/login" element={<IsNotLoggedIn view={Login} />} />
            <Route
              path="/protected"
              element={<IsLoggedIn view={Protected} />}
            />

            <Route path="*" element={<div>No page</div>} />
          </Routes>
        </Layout>
      </AuthContext.Provider>
    </BrowserRouter>
  );
};

export default App;
