const API = "http://localhost:5000";

export const login = async (body: { email: string; password: string }) => {
  const response = await fetch(`${API}/login`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify(body),
  });
  const data = await response.json();

  if (!response.ok) throw new Error(data);

  return data;
};
