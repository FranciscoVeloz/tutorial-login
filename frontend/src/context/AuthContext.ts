import { createContext } from "react";
import { Auth } from "../interface/Auth";
import { User } from "../interface/User";

interface context {
  user?: User | null;
  login: (user: Auth) => void;
  logout: () => void;
  updateSession: (user: User) => void;
}

const AuthContext = createContext<context>({
  user: { email: "", role: "", username: "" },
  login: () => {},
  logout: () => {},
  updateSession: () => {},
});

export default AuthContext;
