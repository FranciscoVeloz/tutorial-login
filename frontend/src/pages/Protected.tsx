const Protected = () => {
  return (
    <div>
      <h1>This is a protected page</h1>
    </div>
  );
};

export default Protected;
