import { useState } from "react";
import { Auth } from "../interface/Auth";
import { User } from "../interface/User";

const useAuth = () => {
  const getUser = () => {
    const session = window.localStorage.getItem("UsuarioProan");
    if (session === undefined || session === null) return null;

    const { user }: Auth = JSON.parse(session);
    return user;
  };

  const [user, setUser] = useState(getUser());

  const login = async (payload: Auth) => {
    try {
      window.localStorage.setItem("UsuarioProan", JSON.stringify(payload));
    } catch (error) {
      console.log(error);
    }
  };

  const logout = async () => {
    window.localStorage.clear();
    setUser(null);
  };

  const updateSession = async (user: User) => {
    const session = window.localStorage.getItem("UsuarioProan");
    const { token }: Auth = JSON.parse(session!);
    const newSession = { token, user };

    window.localStorage.clear();
    setUser(null);

    window.localStorage.setItem("UsuarioProan", JSON.stringify(newSession));
    setUser(user);
  };

  return {
    user,
    login,
    logout,
    updateSession,
  };
};

export default useAuth;
